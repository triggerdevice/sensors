//
//  STHumiditySensor.m
//  Sensors
//
//  Created by Masahiro Murase on 2015/09/20.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import "STHumiditySensor.h"

@implementation STHumiditySensor

- (NSDictionary *)snapshot
{
  return @{
           @"relativeHumidity": self.relativeHumidity,
           };
}

- (NSNumber *)relativeHumidity
{
  NSData *value = self.dataCharacteristic.value;
  if (!value) {
    return @(-0.0);
  }

  int8_t rawBytes[value.length];
  [value getBytes:&rawBytes length:value.length];

  uint16_t hum = (rawBytes[2] & 0xff) | ((rawBytes[3] << 8) & 0xff00);
  float rHVal = -6.0f + 125.0f * (float)((float)hum/(float)65535);

  return @(rHVal);
}

@end
