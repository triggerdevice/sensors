//
//  STHumiditySensor.h
//  Sensors
//
//  Created by Masahiro Murase on 2015/09/20.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import "STSensor.h"

/*
 * JSON
 *
 * Key
 * relativeHumidity
 *
 * Value
 * 浮動小数点数 [%rH]
 */

@interface STHumiditySensor : STSensor

/// 相対湿度 [%rH]
@property (nonatomic, strong, readonly) NSNumber *relativeHumidity;

@end
