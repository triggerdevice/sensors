//
//  STGyroscopeSensor.h
//  Sensors
//
//  Created by Masahiro Murase on 2015/09/20.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import "STSensor.h"

/*
 * JSON
 *
 * Key
 * angularVelocityX
 * angularVelocityY
 * angularVelocityZ
 *
 * Value
 * 浮動小数点数 [deg/s]
 */

@interface STGyroscopeSensor : STSensor

/// X軸角速度 [deg/s]
@property (readonly, nonatomic, strong) NSNumber *x;

/// Y軸角速度 [deg/s]
@property (readonly, nonatomic, strong) NSNumber *y;

/// Z軸角速度 [deg/s]
@property (readonly, nonatomic, strong) NSNumber *z;


/// 現在のセンサー値を起点としてキャリブレーションする
-(void)calibrate;

@end
