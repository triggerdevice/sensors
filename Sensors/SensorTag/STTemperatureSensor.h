//
//  STTemperatureSensor.h
//  Sensors
//
//  Created by Masahiro Murase on 2015/09/20.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import "STSensor.h"

/*
 * JSON
 *
 * Key
 * objctTemperature
 * ambientTemperature
 *
 * Value
 * 浮動小数点数 [°C]
 */

@interface STTemperatureSensor : STSensor

/// 赤外線温度センサーで計測した温度 [℃]
@property (readonly, nonatomic, strong) NSNumber *objctTemperature;

/// 環境温度 [℃]
@property (readonly, nonatomic, strong) NSNumber *ambientTemperature;

@end
