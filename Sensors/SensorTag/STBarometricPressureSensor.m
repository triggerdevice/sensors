//
//  STBarometricPressureSensor.m
//  Sensors
//
//  Created by Masahiro Murase on 2015/09/20.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import "STSensorConfiguration.h"
#import "STBarometricPressureSensor.h"

@interface STBarometricPressureSensor ()
@property (readwrite, nonatomic, weak) CBCharacteristic *calibrationCharacteristic;
@property (nonatomic, copy) NSString *calibrationCharacteristicUUIDString;
@property (nonatomic, assign, getter=isCalibrated) BOOL calibrated;

///Calibration values
@property (nonatomic, assign) uint16_t c1;
@property (nonatomic, assign) uint16_t c2;
@property (nonatomic, assign) uint16_t c3;
@property (nonatomic, assign) uint16_t c4;
@property (nonatomic, assign) int16_t c5;
@property (nonatomic, assign) int16_t c6;
@property (nonatomic, assign) int16_t c7;
@property (nonatomic, assign) int16_t c8;
@end

@implementation STBarometricPressureSensor

- (instancetype)initWithService:(CBService *)service configuration:(STSensorConfiguration *)configuration
{
  self = [super initWithService:service configuration:configuration];
  if (self) {
    self.calibrated = NO;

    NSDictionary *config = [configuration dictionaryFromServiceUUID:service.UUID.UUIDString];
    _calibrationCharacteristicUUIDString = config[kCalibrationCharacteristicUUIDKey];

    for (CBCharacteristic *characteristic in self.service.characteristics) {
      if ([_calibrationCharacteristicUUIDString isEqualToString:characteristic.UUID.UUIDString]) {
        _calibrationCharacteristic = characteristic;
      }
    }

  }
  return self;
}

- (void)sensorDidUpdateValueForCharacteristic:(CBCharacteristic *)characteristic
{
  if ([characteristic.UUID isEqual:self.calibrationCharacteristic.UUID]) {
    
    [self p_setCalibrationData:characteristic.value];
    
    // キャリブレーション終了
    self.calibrated = YES;
  }
}

- (void)sensorDidFinishConfiguration
{
  [self p_calibrate];
  [self p_requestReadCalibrationData];
}

- (NSDictionary *)snapshot
{
  return @{ @"pressure": self.pressure };
}

#pragma mark - Accessor

-(void)setCalibrated:(BOOL)calibrated
{
  _calibrated = calibrated;

  self.enabled = _calibrated;
  self.notificationEnabled = _calibrated;
}

- (NSNumber *)pressure
{
  if (!self.isCalibrated) {
    return @(-0.0f);
  }
  
  NSData *value = self.dataCharacteristic.value;
  if (!value || (value.length < 4)) {
    return @(-0.0);
  }

  int8_t rawBytes[4];
  [value getBytes:&rawBytes length:sizeof(rawBytes)];
  
  int16_t temp = (rawBytes[0] & 0xff) | ((rawBytes[1] << 8) & 0xff00);
  uint16_t pressure= (rawBytes[2] & 0xff) | ((rawBytes[3] << 8) & 0xff00);
  
  // Barometer calculation
  long long S = _c3 + ((_c4 * (long long)temp) / ((long long)1 << 17)) + ((_c5 * ((long long)temp * (long long)temp))/(long long)((long long)1 << 34));
  long long O = (_c6 * ((long long)1 << 14)) + (((_c7 * (long long)temp)/((long long)1 << 3))) + ((_c8 * ((long long)temp * (long long)temp)) / (long long)((long long)1 << 19));
  long long Pa = (((S * (long long)pressure) + O) / (long long)((long long)1 << 14));
  
  
  return @((int)((int)Pa / (int)100));
}

#pragma mark - Private instance methods

-(void)p_calibrate
{
  // Issue calibration to the device
  uint8_t data = 0x02;
  NSData *writeValue = [NSData dataWithBytes:&data length:sizeof(data)];
  [self.service.peripheral writeValue:writeValue forCharacteristic:self.configurationCharacteristic type:CBCharacteristicWriteWithResponse];
}

- (void)p_setCalibrationData:(NSData *)data
{
  uint8_t rawBytes[16];
  [data getBytes:&rawBytes length:sizeof(rawBytes)];

  _c1 = ((rawBytes[0] & 0xff) | ((rawBytes[1] << 8) & 0xff00));
  _c2 = ((rawBytes[2] & 0xff) | ((rawBytes[3] << 8) & 0xff00));
  _c3 = ((rawBytes[4] & 0xff) | ((rawBytes[5] << 8) & 0xff00));
  _c4 = ((rawBytes[6] & 0xff) | ((rawBytes[7] << 8) & 0xff00));
  _c5 = ((rawBytes[8] & 0xff) | ((rawBytes[9] << 8) & 0xff00));
  _c6 = ((rawBytes[10] & 0xff) | ((rawBytes[11] << 8) & 0xff00));
  _c7 = ((rawBytes[12] & 0xff) | ((rawBytes[13] << 8) & 0xff00));
  _c8 = ((rawBytes[14] & 0xff) | ((rawBytes[15] << 8) & 0xff00));
}

- (void)p_requestReadCalibrationData
{
  [self.service.peripheral readValueForCharacteristic:_calibrationCharacteristic];
}
@end
