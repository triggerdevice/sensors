//
//  STSensorTag.m
//  Sensors
//
//  Created by Masahiro Murase on 2015/09/19.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

@import CoreBluetooth;
#import "STSensor.h"
#import "STSensor+Factory.h"
#import "STSensorConfiguration.h"
#import "STSensorTag.h"

static NSString * const kSensorTagShortLocalName = @"SensorTag";
static NSString * const kSensorTagLongLocalName = @"TI BLE Sensor Tag";


@interface STSensorTag () <CBPeripheralDelegate>
@property (readwrite, nonatomic, strong) CBPeripheral *peripheral;
@property (nonatomic, strong) NSMutableArray *sensors;
@end

@implementation STSensorTag

- (instancetype)initWithPeripheral:(CBPeripheral *)peripheral;
{
  self = [super init];
  if (self) {
    _peripheral = peripheral;
    _peripheral.delegate = self;
    _sensors = [NSMutableArray array];
  }
  return self;
}

/// センサーを追加する
-(void)addSensor:(STSensor *)sensor
{
  [_sensors addObject:sensor];
}

/// 指定したService UUIDを持つセンサーを探す
- (STSensor *)findSensorWithServiceUUID:(NSString *)serviceUUIDString
{
  for (STSensor *sensor in _sensors) {
    if ([sensor.service.UUID.UUIDString isEqualToString:serviceUUIDString]) {
      return sensor;
    }
  }
  return nil;
}

/// 指定したCharacteristic UUIDを持つセンサーを探す
- (STSensor *)findSensorWithCharacteristicUUID:(NSString *)characteristicUUIDString
{
  for (STSensor *sensor in _sensors) {
    if ([sensor hasCharacteristicWithUUID:characteristicUUIDString]) {
      return sensor;
    }
  }
  return nil;
}

- (NSString *)takeJSONSnapshot
{
  NSMutableDictionary *snapshot = [NSMutableDictionary dictionary];

  for (STSensor *sensor in _sensors) {
    [snapshot addEntriesFromDictionary:[sensor snapshot]];
  }

  NSData *data = [NSJSONSerialization dataWithJSONObject:snapshot options:0 error:NULL];

  return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
}

+ (BOOL)isSensorTagName:(NSString *)localName
{
  return ([localName isEqualToString:kSensorTagShortLocalName] ||
          [localName isEqualToString:kSensorTagLongLocalName]);
}

- (void)calibrateMagnetometerSensor
{
  [_sensors enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop){
    STSensor *sensor = obj;
    if ([sensor isKindOfClass:[NSClassFromString(@"STMagnetometerSensor") class]]) {
      [sensor calibrate];
      *stop = YES;
      return;
    }
  }];
}

- (void)calibrateGyroscopeSensor
{
  [_sensors enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop){
    STSensor *sensor = obj;
    if ([sensor isKindOfClass:[NSClassFromString(@"STGyroscopeSensor") class]]) {
      [sensor calibrate];
      *stop = YES;
      return;
    }
  }];
}


#pragma mark - <CBPeripheralDelegate>

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
  NSLog(@"%s, found services:%@", __PRETTY_FUNCTION__, peripheral.services);
  
  [peripheral.services enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
    CBService *service = obj;
    [peripheral discoverCharacteristics:nil
                             forService:service];
  }];
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
  NSLog(@"%s, found characteristics:%@", __PRETTY_FUNCTION__, service.characteristics);
  
  STSensorConfiguration *configuration = [STSensorConfiguration defaultConfiguration];
  STSensor *sensor = [STSensor sensorWithService:service
                                   configuration:configuration];
  if (sensor) {
    [self addSensor:sensor];
  }
  
  __block BOOL allDiscovered = YES;
  [peripheral.services enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
    CBService *service = obj;
    if (!service.characteristics) {
      allDiscovered = NO;
      *stop = YES;
      return;
    }
  }];
  if (allDiscovered) {
    NSLog(@"complete!");
    [self p_enableSensors:configuration];
  }
}

- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
  NSLog(@"%s, characteristic:%@, error:%@", __PRETTY_FUNCTION__, characteristic.UUID.UUIDString, [error localizedDescription]);
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
//  NSLog(@"%s, characteristic:%@, error:%@", __PRETTY_FUNCTION__, characteristic.UUID.UUIDString, [error localizedDescription]);
  
  STSensor *sensor = [self findSensorWithCharacteristicUUID:characteristic.UUID.UUIDString];
  [sensor sensorDidUpdateValueForCharacteristic:characteristic];
  
//  NSLog(@"%@", sensor);
}

#pragma mark - Private instance methods

- (void)p_enableSensors:(STSensorConfiguration *)configuration
{
  [_peripheral.services enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
    
    CBService *service = obj;
    
    STSensor *sensor = [self findSensorWithServiceUUID:service.UUID.UUIDString];
    if (!sensor) {
      return;
    }

    NSDictionary *configurationDic = [configuration dictionaryFromServiceUUID:service.UUID.UUIDString];
    float period = configurationDic[kPeriodKey] ? [configurationDic[kPeriodKey] floatValue] : [STSensorConfiguration defaultPeriod];
    [sensor setPeriod:period];
    
    sensor.enabled = YES;
    sensor.notificationEnabled = YES;
    [sensor sensorDidFinishConfiguration];
  }];
}

@end
