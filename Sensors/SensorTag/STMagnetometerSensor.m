//
//  STMagnetometerSensor.m
//  Sensors
//
//  Created by Masahiro Murase on 2015/09/20.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import "STMagnetometerSensor.h"

#define MAG3110_RANGE 2000.0

@interface STMagnetometerSensor ()
@property (nonatomic, assign) float lastX;
@property (nonatomic, assign) float lastY;
@property (nonatomic, assign) float lastZ;
@property (nonatomic, assign) float calibratedX;
@property (nonatomic, assign) float calibratedY;
@property (nonatomic, assign) float calibratedZ;
@end

@implementation STMagnetometerSensor

- (instancetype)initWithService:(CBService *)service configuration:(STSensorConfiguration *)configuration
{
  self = [super initWithService:service configuration:configuration];
  if (self) {
    _lastX = 0.0f;
    _lastY = 0.0f;
    _lastZ = 0.0f;
    _calibratedX = 0.0f;
    _calibratedY = 0.0f;
    _calibratedZ = 0.0f;
  }
  return self;
}

- (NSDictionary *)snapshot
{
  return @{
           @"magneticFieldX": self.x,
           @"magneticFieldY": self.y,
           @"magneticFieldZ": self.z,
           };
}

- (NSNumber *)x
{
  NSData *value = self.dataCharacteristic.value;
  if (!value) {
    return @(-0.0);
  }
  
  int8_t rawBytes[value.length];
  [value getBytes:&rawBytes length:sizeof(rawBytes)];

  int16_t rawX = (rawBytes[0] & 0xff) | ((rawBytes[1] << 8) & 0xff00);
  self.lastX = (((float)rawX * 1.0) / ( 65536 / MAG3110_RANGE )) * -1;

  return @(_lastX - _calibratedX);
}

- (NSNumber *)y
{
  NSData *value = self.dataCharacteristic.value;
  if (!value) {
    return @(-0.0);
  }
  
  int8_t rawBytes[value.length];
  [value getBytes:&rawBytes length:sizeof(rawBytes)];

  int16_t rawY = ((rawBytes[2] & 0xff) | ((rawBytes[3] << 8) & 0xff00));
  self.lastY = (((float)rawY * 1.0) / ( 65536 / MAG3110_RANGE )) * -1;

  return @(_lastY - _calibratedY);
}

- (NSNumber *)z
{
  NSData *value = self.dataCharacteristic.value;
  if (!value) {
    return @(-0.0);
  }
  
  int8_t rawBytes[value.length];
  [value getBytes:&rawBytes length:sizeof(rawBytes)];

  int16_t rawZ = (rawBytes[4] & 0xff) | ((rawBytes[5] << 8) & 0xff00);
  self.lastZ =  ((float)rawZ * 1.0) / ( 65536 / MAG3110_RANGE );

  return @(_lastZ - _calibratedZ);
}

-(void)calibrate
{
  self.calibratedX = _lastX;
  self.calibratedY = _lastY;
  self.calibratedZ = _lastZ;
}
@end
