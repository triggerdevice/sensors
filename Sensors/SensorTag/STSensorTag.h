//
//  STSensorTag.h
//  Sensors
//
//  Created by Masahiro Murase on 2015/09/19.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CBPeripheral;
@class STSensor;

@interface STSensorTag : NSObject

/// SensorTag端末
@property (readonly, nonatomic, strong) CBPeripheral *peripheral;

/// 関連づける(ペリフェラル)センサータグを指定してインスタンスを初期化する
- (instancetype)initWithPeripheral:(CBPeripheral *)peripheral NS_DESIGNATED_INITIALIZER;

/// デバイスが持つセンサーの最新センサー値をJSON形式文字列で取得する
- (NSString *)takeJSONSnapshot;

/// 指定された文字列がセンサータグのローカルネームに一致するかどうかを調べる
+ (BOOL)isSensorTagName:(NSString *)localName;

- (void)calibrateMagnetometerSensor;
- (void)calibrateGyroscopeSensor;

- (instancetype)init __attribute__((unavailable("unavailable initializer")));

@end
