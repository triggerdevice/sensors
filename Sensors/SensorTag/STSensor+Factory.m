//
//  STSensor+Factory.m
//  Sensors
//
//  Created by Masahiro Murase on 2015/09/20.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import "STSensorConfiguration.h"
#import "STSensor+Factory.h"

@implementation STSensor (Factory)

+ (STSensor *)sensorWithService:(CBService *)service configuration:(STSensorConfiguration *)configuration
{
  NSDictionary *sensorConfiguraiton = [configuration dictionaryFromServiceUUID:service.UUID.UUIDString];
  if (!sensorConfiguraiton) {
    return nil;
  }

  NSDictionary * const classNames = @{
                                      kIRTemperatureSensorNameKey:      @"STTemperatureSensor",
                                      kAccelerometerSensorNameKey:      @"STAccelerometerSensor",
                                      kHumiditySensorNameKey:           @"STHumiditySensor",
                                      kMagnetometerSensorNameKey:       @"STMagnetometerSensor",
                                      kBarometricPressureSensorNameKey: @"STBarometricPressureSensor",
                                      kGyroscopeSensorNameKey:          @"STGyroscopeSensor",
                                      };

  NSString *className = classNames[sensorConfiguraiton[kSensorNameKey]];
#if 0
  NSString *className = nil;

  if ([sensorConfiguraiton[kSensorNameKey] isEqualToString:kIRTemperatureSensorNameKey]) {
    className = @"STTemperatureSensor";
  }
  else if ([sensorConfiguraiton[kSensorNameKey] isEqualToString:kAccelerometerSensorNameKey]) {
    className = @"STAccelerometerSensor";
  }
  else if ([sensorConfiguraiton[kSensorNameKey] isEqualToString:kHumiditySensorNameKey]) {
    className = @"STHumiditySensor";
  }
  else if ([sensorConfiguraiton[kSensorNameKey] isEqualToString:kMagnetometerSensorNameKey]) {
    className = @"STMagnetometerSensor";
  }
  else if ([sensorConfiguraiton[kSensorNameKey] isEqualToString:kBarometricPressureSensorNameKey]) {
    className = @"STBarometricPressureSensor";
  }
  else if ([sensorConfiguraiton[kSensorNameKey] isEqualToString:kGyroscopeSensorNameKey]) {
    className = @"STGyroscopeSensor";
  }
  else {
  }
#endif
  
  return [[NSClassFromString(className) alloc] initWithService:service configuration:configuration];
}

@end
