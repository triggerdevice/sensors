//
//  STAccelerometerSensor.m
//  Sensors
//
//  Created by Masahiro Murase on 2015/09/20.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import "STAccelerometerSensor.h"

#define KXTJ9_RANGE (4.0)

@implementation STAccelerometerSensor

- (NSDictionary *)snapshot
{
  return @{
           @"accelerationX": self.x,
           @"accelerationY": self.y,
           @"accelerationZ": self.z,
           };
}

- (NSNumber *)x
{
  NSData *value = self.dataCharacteristic.value;
  if (!value) {
    return @(-0.0);
  }

  int8_t rawBytes[value.length];
  [value getBytes:&rawBytes length:sizeof(rawBytes)];

  return @((rawBytes[0] * 1.0) / (256 / KXTJ9_RANGE));
}

- (NSNumber *)y
{
  NSData *value = self.dataCharacteristic.value;
  if (!value) {
    return @(-0.0);
  }

  //Orientation of sensor on board means we need to swap Y (multiplying with -1)
  int8_t rawBytes[value.length];
  [value getBytes:&rawBytes length:sizeof(rawBytes)];

  return @(((rawBytes[1] * 1.0) / (256 / KXTJ9_RANGE)) * -1);
}

- (NSNumber *)z
{
  NSData *value = self.dataCharacteristic.value;
  if (!value) {
    return @(-0.0);
  }

  int8_t rawBytes[value.length];
  [value getBytes:&rawBytes length:sizeof(rawBytes)];

  return @((rawBytes[2] * 1.0) / (256 / KXTJ9_RANGE));
}

@end
