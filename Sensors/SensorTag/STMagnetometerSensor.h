//
//  STMagnetometerSensor.h
//  Sensors
//
//  Created by Masahiro Murase on 2015/09/20.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import "STSensor.h"

/*
 * JSON
 *
 * Key
 * magneticFieldX
 * magneticFieldY
 * magneticFieldZ
 *
 * Value
 * 浮動小数点数 [μT]
 */

@interface STMagnetometerSensor : STSensor

/// X軸地磁気 [μT]
@property (readonly, nonatomic, strong) NSNumber *x;

/// Y軸地磁気 [μT]
@property (readonly, nonatomic, strong) NSNumber *y;

/// Z軸地磁気 [μT]
@property (readonly, nonatomic, strong) NSNumber *z;


/// 現在のセンサー値を起点としてキャリブレーションする
-(void)calibrate;

@end
