//
//  STBarometricPressureSensor.h
//  Sensors
//
//  Created by Masahiro Murase on 2015/09/20.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import "STSensor.h"

/*
 * JSON
 *
 * Key
 * pressure
 *
 * Value
 * 整数 [hPa]
 */

@interface STBarometricPressureSensor : STSensor
@property (readonly, nonatomic, weak) CBCharacteristic *calibrationCharacteristic;

/// 大気圧 [hPa]
@property (readonly, nonatomic, strong) NSNumber *pressure;

@end
