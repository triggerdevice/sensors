//
//  STAccelerometerSensor.h
//  Sensors
//
//  Created by Masahiro Murase on 2015/09/20.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import "STSensor.h"

/*
 * JSON
 *
 * Key
 * accelerationX
 * accelerationY
 * accelerationZ
 *
 * Value
 * 浮動小数点数 [G]
 */

@interface STAccelerometerSensor : STSensor

// 範囲は +/-2G

/// X軸加速度 [G]
@property (readonly, nonatomic, strong) NSNumber *x;

/// Y軸加速度 [G]
@property (readonly, nonatomic, strong) NSNumber *y;

/// Z軸加速度 [G]
@property (readonly, nonatomic, strong) NSNumber *z;

@end
