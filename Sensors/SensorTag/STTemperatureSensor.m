//
//  STTemperatureSensor.m
//  Sensors
//
//  Created by Masahiro Murase on 2015/09/20.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import "STTemperatureSensor.h"

@interface STTemperatureSensor ()
@end

@implementation STTemperatureSensor

- (NSDictionary *)snapshot
{
  return @{
           @"objctTemperature": self.objctTemperature,
           @"ambientTemperature": self.ambientTemperature,
           };
}

- (NSNumber *)objctTemperature
{
  NSData *value = self.dataCharacteristic.value;
  if (!value) {
    return @(-0.0);
  }

  int8_t rawBytes[value.length];
  [value getBytes:&rawBytes length:sizeof(rawBytes)];

  int16_t objTemp = (rawBytes[0] & 0xff)| ((rawBytes[1] << 8) & 0xff00);
  int16_t ambTemp = ((rawBytes[2] & 0xff)| ((rawBytes[3] << 8) & 0xff00));
  
  float temp = (float)((float)ambTemp / 128.f);
  long double Vobj2 = (double)objTemp * .00000015625;
  long double Tdie2 = (double)temp + 273.15;
  long double S0 = 6.4 * pow(10, -14);
  long double a1 = 1.75 * pow(10, -3);
  long double a2 = -1.678 * pow(10, -5);
  long double b0 = -2.94 * pow(10, -5);
  long double b1 = -5.7 * pow(10, -7);
  long double b2 = 4.63 * pow(10, -9);
  long double c2 = 13.4f;
  long double Tref = 298.15;
  long double S = S0 * (1 + a1 * (Tdie2 - Tref) + a2 * pow((Tdie2 - Tref), 2));
  long double Vos = b0 + b1 * (Tdie2 - Tref) + b2 * pow((Tdie2 - Tref), 2);
  long double fObj = (Vobj2 - Vos) + c2 * pow((Vobj2 - Vos), 2);
  long double Tobj = pow(pow(Tdie2, 4) + (fObj / S), .25);
  Tobj = (Tobj - 273.15);

  return @((float)Tobj);
}

- (NSNumber *)ambientTemperature
{
  NSData *value = self.dataCharacteristic.value;
  if (!value) {
    return @(-0.0);
  }

  int8_t rawBytes[value.length];
  [value getBytes:&rawBytes length:sizeof(rawBytes)];

  int16_t ambTemp = ((rawBytes[2] & 0xff)| ((rawBytes[3] << 8) & 0xff00));
  
  return @((float)((float)ambTemp / 128.f));
}

@end
