//
//  SensorValueTransmitter.h
//  Sensors
//
//  Created by Masahiro Murase on 2015/09/23.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SensorValueTransmitter : NSObject

/// 接続するサーバー
@property (nonatomic, copy) NSString *host;

/// メッセージを送信するtriggerのUUID
@property (nonatomic, copy) NSString *triggerUUID;

/// メッセージを送信するtriggerのtoken
@property (nonatomic, copy) NSString *triggerToken;

/// メッセージを転送するactionのUUID文字列
@property (nonatomic, copy) NSArray *devices;

- (instancetype)initWithHost:(NSString *)host triggerUUID:(NSString *)triggerUUID triggerToken:(NSString *)triggerToken devices:(NSArray *)devices;

- (void)postSensorValues:(NSDictionary *)values success:(void (^)(void))successBlock failure:(void (^)(NSError *error))failureBlock;

@end
