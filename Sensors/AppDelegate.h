//
//  AppDelegate.h
//  Sensors
//
//  Created by Masahiro Murase on 2015/09/21.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AppSettings;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (readonly, nonatomic, strong) AppSettings *appSettings;

@end

