//
//  AppSettings+Import.m
//  BeaconMonitor
//
//  Created by Masahiro Murase on 2015/09/27.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import "AppSettings+Import.h"

static NSString * const kTriggerPrefix = @"trigger-";
static NSString * const kActionPrefix = @"action-";
static NSString * const kJSONKeywordKey = @"keyword";
static NSString * const kJSONUUIDKey = @"uuid";
static NSString * const kJSONTokenKey = @"token";
static NSString * const kUserDefaultsTriggerUUIDFormat = @"meshblu_trigger-%zd_uuid";
static NSString * const kUserDefaultsTriggerTokenFormat = @"meshblu_trigger-%zd_token";
static NSString * const kUserDefaultsActionUUIDFormat = @"meshblu_action-%zd_uuid";
static NSString * const kUserDefaultsActionTokenFormat = @"meshblu_action-%zd_token";


@implementation AppSettings (Import)

/**
 追加でサポートするUserDefaultsのキー。
 定義したキーが見つかった場合、UserDefaultsに同名のキーがあればvalue値とともに上書きし、無い場合は追加する。
 */
+ (NSArray *)supportedOtherKeys
{
  // 使っているキーの情報は RegisterDefaults.json を参照
  return @[@"host"];
}

/**
 JSON文字列を取り込んでUserDefaultsのデバイス情報を上書きする
 trigger-1 は meshblu_trigger-1_uuid、action-2 は meshblu_action-2_uuid のようにブリッジする
 */
+ (void)importMeshbluDevicesFromJSONString:(NSString *)aJSONstring
{
  NSData *jsonData = [aJSONstring dataUsingEncoding:NSUTF8StringEncoding];
  
  NSError *error = nil;
  NSArray *array = [NSJSONSerialization JSONObjectWithData:jsonData
                                                   options:NSJSONReadingAllowFragments
                                                     error:&error];

  if (error || ![array isKindOfClass:[NSArray class]]) {
    return;
  }
  
  NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
  
  for (NSDictionary *device in array) {
    if (![device isKindOfClass:[NSDictionary class]]) {
      continue;
    }

    NSString *keyword = device[kJSONKeywordKey];
    
    if ([keyword hasPrefix:kTriggerPrefix]) {
      NSInteger index = [keyword stringByReplacingOccurrencesOfString:kTriggerPrefix withString:@""].integerValue;
      
      NSString *uuid = device[kJSONUUIDKey];
      if (uuid && ![uuid isEqualToString:@""]) {
        NSString *key = [NSString stringWithFormat:kUserDefaultsTriggerUUIDFormat, index];
        [ud setObject:uuid forKey:key];
      }
      
      NSString *token = device[kJSONTokenKey];
      if (token && ![token isEqualToString:@""]) {
        NSString *key = [NSString stringWithFormat:kUserDefaultsTriggerTokenFormat, index];
        [ud setObject:token forKey:key];
      }
    }
    else if ([device[kJSONKeywordKey] hasPrefix:kActionPrefix]) {
      NSInteger index = [keyword stringByReplacingOccurrencesOfString:kActionPrefix withString:@""].integerValue;
      
      NSString *uuid = device[kJSONUUIDKey];
      if (uuid && ![uuid isEqualToString:@""]) {
        NSString *key = [NSString stringWithFormat:kUserDefaultsActionUUIDFormat, index];
        [ud setObject:uuid forKey:key];
      }
      
      NSString *token = device[kJSONTokenKey];
      if (token && ![token isEqualToString:@""]) {
        NSString *key = [NSString stringWithFormat:kUserDefaultsActionTokenFormat, index];
        [ud setObject:token forKey:key];
      }
    }
    else {
      [device enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop){
        if ([self.supportedOtherKeys containsObject:key]) {
          [ud setObject:obj forKey:key];
        }
      }];
    }
  }
  [ud synchronize];
}

@end
