//
//  QRCodeReaderViewController.m
//  BeaconMonitor
//
//  Created by Masahiro Murase on 2015/09/27.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

@import AVFoundation;
#import "QRCodeReaderViewController.h"

@interface QRCodeReaderViewController () <AVCaptureMetadataOutputObjectsDelegate>
@property (nonatomic, strong) AVCaptureStillImageOutput *stillImageOutput;

@property (nonatomic, strong) AVCaptureVideoPreviewLayer *previewLayer;
@property (nonatomic, strong) AVCaptureSession *session;
@property (nonatomic, strong) AVCaptureDevice *backCamera;
@property (nonatomic, strong) AVCaptureDeviceInput *cameraInput;

@property (readwrite, nonatomic, copy) NSString *detectString;


// Actions & Outlets
@property (weak, nonatomic) IBOutlet UITextView *resultTextView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *importButton;
- (IBAction)closeButtonDidPush:(id)sender;
- (IBAction)importButtonDidPush:(id)sender;

@end

@implementation QRCodeReaderViewController

- (void)viewDidLoad
{
  [super viewDidLoad];
  // Do any additional setup after loading the view.

  self.importButton.enabled = NO;
  self.resultTextView.hidden = YES;

  [self p_configureCamera];
}

- (void) viewDidLayoutSubviews
{
  // resize layers based on the view's new frame
  self.previewLayer.frame = self.view.bounds;
}

- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (void)p_configureCamera
{
  self.session = [[AVCaptureSession alloc] init];
  
  AVCaptureDevice *frontCamera = nil;
  AVCaptureDevice *targetDevice = nil;
  NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
  for (AVCaptureDevice *device in devices) {
    if (device.position == AVCaptureDevicePositionBack) {
      targetDevice = device;
    }
    else if (device.position == AVCaptureDevicePositionFront) {
      frontCamera = device;
    }
  }
  if (!targetDevice) {
    if (!frontCamera) {
      return;
    }
    targetDevice = frontCamera;
  }
  self.backCamera = targetDevice;

  
  self.cameraInput = [[AVCaptureDeviceInput alloc] initWithDevice:_backCamera
                                                            error:NULL];
  if ([_session canAddInput:_cameraInput]) {
    [_session addInput:_cameraInput];
  }
  

  self.stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
  if (self.stillImageOutput.isStillImageStabilizationSupported) {
    self.stillImageOutput.automaticallyEnablesStillImageStabilizationWhenAvailable = YES;
  }
  if ([_session canAddOutput:_stillImageOutput]) {
    [_session addOutput:_stillImageOutput];
  }

  
  AVCaptureMetadataOutput *metaOutput = [[AVCaptureMetadataOutput alloc] init];
  [metaOutput setMetadataObjectsDelegate:self
                                   queue:dispatch_queue_create("com.triggerdevice.metadataQueue", DISPATCH_QUEUE_SERIAL)];
  [_session addOutput:metaOutput];
  metaOutput.metadataObjectTypes = @[AVMetadataObjectTypeQRCode];
  
  
  _previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:_session];
  _previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
  [self.view.layer insertSublayer:_previewLayer atIndex:0];

  [_session startRunning];
}

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection
{
  for (AVMetadataObject *metadataObject in metadataObjects) {
    if (![metadataObject isKindOfClass:[AVMetadataMachineReadableCodeObject class]]) {
      continue;
    }
    self.detectString = [(AVMetadataMachineReadableCodeObject *)metadataObject stringValue];
    
    if ([metadataObject.type isEqualToString:AVMetadataObjectTypeQRCode]) {
      [_session stopRunning];
      dispatch_async(dispatch_get_main_queue(), ^{
        self.importButton.enabled = YES;
        self.resultTextView.hidden = NO;
        self.resultTextView.text = _detectString;
      });
    }
  }
}

- (IBAction)closeButtonDidPush:(id)sender
{
  if ([_delegate respondsToSelector:@selector(qrCodeReaderDidTapCloseButton:)]) {
    [_delegate qrCodeReaderDidTapCloseButton:self];
  }
}
- (IBAction)importButtonDidPush:(id)sender
{
  if ([_delegate respondsToSelector:@selector(qrCodeReaderDidTapImportButton:)]) {
    [_delegate qrCodeReaderDidTapImportButton:self];
  }
}
@end
