//
//  AppSettings+Import.h
//  BeaconMonitor
//
//  Created by Masahiro Murase on 2015/09/27.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import "AppSettings.h"

@interface AppSettings (Import)

/**
 JSON形式のMeshblu設定ファイルを読み込んでUserDefaults設定を上書きする
 サポートするJSONは
 {"keyword": キーワード文字列, "uuid": UUID文字列, "token": token文字列}
 の形式の辞書が複数配列状に定義されたもの。
 */
+ (void)importMeshbluDevicesFromJSONString:(NSString *)aJSONstring;

@end
